package com.swaggerapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swaggerapp.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}

package com.swaggerapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.swaggerapp.model.Customer;
import com.swaggerapp.repo.CustomerRepository;
import com.swaggerapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private CustomerRepository customerRepository;

	@Transactional(readOnly = true)
	public List<Customer> getCustomers(){
		return customerRepository.findAll();
	}

	@Transactional
	public Customer addCustomer(Customer customer) {

		return customerRepository.save(customer);
	}

}

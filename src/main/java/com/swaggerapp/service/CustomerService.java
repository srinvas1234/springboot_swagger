package com.swaggerapp.service;

import java.util.List;

import com.swaggerapp.model.Customer;

public interface CustomerService {

	List<Customer> getCustomers();
	
	public Customer addCustomer(Customer customer); 
}

package com.swaggerapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swaggerapp.model.Customer;
import com.swaggerapp.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/all")
	public List<Customer> getAllCust(){
		return customerService.getCustomers();
		
	}
	@PostMapping("/add")
	public Customer addCust(@RequestBody Customer customer) {
	
		return customerService.addCustomer(customer);
		
}
}
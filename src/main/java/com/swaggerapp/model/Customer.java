package com.swaggerapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="customer")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cust_Id")
	private Long id;
	@Column(name="cust_Name")
	private String custName;
	@Column(name="cust_Address")
	private String custAddress;
	public Customer() {
		super();
	}
	public Customer(Long id, String custName, String custAddress) {
		super();
		this.id = id;
		this.custName = custName;
		this.custAddress = custAddress;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustAddress() {
		return custAddress;
	}
	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", custName=" + custName + ", custAddress=" + custAddress + "]";
	}
	

}
